# Setting up CPU isolation

added isolcpu=3 to /boot/cmdline.txt

sudo su

pushd /sys/fs/cgroup/cpuset
mkdir isolated
mkdir not_rt
echo 0-2 > not_rt/cpuset.cpus
echo 0 > not_rt/cpuset.mems
echo 1 > not_rt/cpuset.sched_load_balance
echo 1 > not_rt/cpuset.cpu_exclusive
echo 3 > isolated/cpuset.cpus
echo 0 > isolated/cpuset.mems
echo 1 > isolated/cpuset.sched_load_balance
echo 1 > isolated/cpuset.cpu_exclusive
echo 0 > cpuset.sched_load_balance
popd


./gpio_4_100000 &

echo $(ps -ef | ag gpio_4 | ag -v ag | awk '{print $2}') > /sys/fs/cgroup/cpuset/isolated/tasks

./gpio_17_700000 &

echo $(ps -ef | ag gpio_17 | ag -v ag | awk '{print $2}') > /sys/fs/cgroup/cpuset/isolated/tasks


cat /sys/fs/cgroup/cpuset/isolated/tasks


trace-cmd record -e all -o just_gpio.dat

kernelshark just_gpio.data

stress-ng -c 1 -k &

echo $(ps -ef | ag stress | ag -v ag | awk '{print $2}') > /sys/fs/cgroup/cpuset/isolated/tasks

trace-cmd record -e all -o gpio_and_stress.dat

kernelshark gpio_and_stress.dat